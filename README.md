# Trackvisualisierung

![Bild](https://codeberg.org/MDradelt/Trackvisualisierung/raw/branch/master/img/20201026_FaFaFa_Beispieldatei_Magdeburg_Olvenstedter_Graseweg_und_Hundisburger_Straße.png)

## Aufbau der Trackdatei im csv-Format

Beispieldatei:
```
type,date,time,latitude,longitude,speed,shockmax,shockavg,name
T,20200601,11550100,52.13302,11.60441,5.5,3,1.1,Segment 1
T,20200601,11550200,52.13306,11.60440,5.5,3.5,1.4
T,20200601,11550300,52.13311,11.60439,5.5,3.3,1.2
```

Zu Unterscheidung der Tracksegmente kann der Zeilenkopf mehrfach eingefügt werden.

|Variable |  Format | Einheit |Bemerkung|
|---------|---------|---------|---------|
|type     |    	    |	      |T für Track|
|date     |YYYYMMDD |	      |Datum aus empfangenem GPS-Datensatz|
|time     |hhmmss.ss| UTC     |Zeit aus empfangenem GPS-Datensatz|
|latitude |	    |         |Breitengrad aus empfangenem GPS-Datensatz|
|longitude|	    |         |Längengrad aus empfangenem GPS-Datensatz|
|speed    |#.#      | m/s     |Geschwindigkeit aus empfangenem GPS-Datensatz|
|shockmax |#.##	    | m/s²    |Maximalbeschleunigung errechnet aus Einzelvektoren|
|shockavg |#.##     | m/s²    |durchschnittliche Beschleunigung|
|name     |test     |         |Freitextfeld zur manuellen Trackabschnittbenennung|

Dateien in diesem Format können direkt bei GPSVisualizer heraufgeladen werden.
Minimalangaben sind `T` für Trackpunkt, Längen- und Breitengrad, sowie die auszuwertende Variable, in diesem Fall `shockmax`

## Voreingestellter Link
https://www.gpsvisualizer.com/map_input?colorize_gray=0&colorize_max=10&colorize_min=0&colorize_reverse=1&form=leaflet&hue1=120&hue2=0&legend_steps=7&trk_colorize=custom&trk_colorize_custom=shockmax&trk_width=5&units=metric

### Voreinstellungen

Voreinstellungen für die direkte Auswertung von aufgezeichneten Tracks mit GPSVisualizer.com

Voreingestellt ist:
* [Leaflet](https://de.wikipedia.org/wiki/Leaflet)
* OpenStreetMap + relief
* Linienbreite: 5
* Farbskala
* auszuwertender Wert für die Linienfarbe: "shockmax"
* Farbskala umgedreht (rot=hoch; grün=tief)
* Werteskala 1-10
* höhere Werte wie 10 genauso wie 10 darstellen

## Beispielbild
![Bild](https://codeberg.org/MDradelt/Trackvisualisierung/raw/commit/db1e6ce63642b6f6fc6431d93df9d15dfeba785a/20201011_Magdeburg_Europaring_Radwegkomfort.jpg)

## Beispiellink
Unter dem folgenden Link wird nach dem Ausführen der Schaltfläche "Draw the Map" die hier hinterlegte Beispieldatei geladen und als zoombare Karte angezeigt. Mit einem Klick auf "View" in der Ergebnisseite lässt sich die Karte im Vollbild darstellen.

https://www.gpsvisualizer.com/map_input?colorize_gray=0&colorize_max=10&colorize_min=0&colorize_reverse=1&form=leaflet&hue1=120&hue2=0&legend_steps=7&trk_colorize=custom&trk_colorize_custom=shockmax&trk_width=5&units=metric&form:remote_data=https://codeberg.org/MDradelt/Trackvisualisierung/raw/branch/master/Beispieldatei%20Lange%20Lake%20s%c3%bcdw%c3%a4rts%2020210329%20Herrenkrugbr%c3%bccke%20bis%20Heisenbergstr.log

## Lizenz/Licence

 <p xmlns:cc="http://creativecommons.org/ns#" ><a rel="cc:attributionURL" href="https://codeberg.org/MDradelt/Trackvisualisierung/">This work</a> by <span property="cc:attributionName">MDradelt</span> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p> 